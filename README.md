# `els`: Emacs Ludicrous Speed

> ### **Emacs start-up in the blink of an eye, with elisp eagerly loaded.**

[Installation instructions are below](#installing). I recommend understanding what it is before using it.

## Important limitations
 * *Linux only* (likely forever)
 * Graphical XWindows emacs only (right now)
 * Only supports a single linux user (right now)
 * A lot of other feasible features are [not implemented](#room-for-improvement)

## Alternative approaches

This is an alternative to other ways of reducing emacs load time, such as:

* client/server
* deferred loading
* emacs dump
* faster hardware :smile:

It is very different from these approaches. In general, it is designed to replace them, not to supplement them.

### Some key differences:

* This is the fastest, I claim. Also, you can add lots of packages and not notice any additional latency.
* vs client/server
  * Each emacs instance is isolated. This is what I want, but it [may or may not be what you want.](#isolation)
  * This uses more memory, of course.
  * If you like running a single server, you could use `els` to start/restart your one server faster
  * `els` provides the best of both worlds if you desire the isolation of multiple emacsen but the speed of opening a new frame in a running emacs
* vs deferred elisp evaluation
  * Simpler config, and emacs will be snappier while you are using it.
  * This uses more memory
* vs emacs dump
  * The `els` approach is *not* portable
  * You do not need to build emacs to use `els`, but the emacs dump features are part of the build process
  * To make a new snapshot with your current config, you run `els-rebake`, and that snapshot will have all your packages eagerly loaded
  * someone who knows more about this may care to comment: there is a [new portable dump feature of Emacs 27](https://archive.casouri.cat/note/2020/painless-transition-to-portable-dumper/)

## What kinda voodoo is this?

[CRIU](https://criu.org/) can snapshot and restore running linux processes. It has many applications, one of which is speeding up
programs which are slow to initialize. It does not support the X window system.
However, Emacs supports running as a client/server, so a server process can be running in the background without displaying any frames (X windows).

So it is possible to keep a snapshot of a freshly started `emacs --daemon` process which has just finished evaluating your entire elisp config eagerly, and to restore that snapshot many times (producing many `emacs --daemon` processes, each listening on its own socket), and then to forward commands to those.

## This project includes:

* The [`els-cloner`](#els-cloner-service) service, which handles the complexity of this for you (it wraps `criu`)
* A SysV init script
* A few helper scripts, which generally just pass messages to the `els-cloner` service

## Precautions

CRIU has to run as `root`, and the `els-cloner` service will also run as `root`.

:warning: [This is free software and there aint no warranty.](LICENSE) :warning:

Your cloned `emacs` instances will still run as a normal user, just as if you launched them without `els`.

## Requirements

* A modern linux kernel (tested 4.19.0)
* A modern emacs (tested 26.1 and the latest [`feature/native-comp`](https://github.com/emacs-mirror/emacs/tree/feature/native-comp) `907618b3b51`)

## Installing

> This project is young, and installation is fairly DIY. If these instructions do not work for you, please help me make them better ([open an issue](https://gitlab.com/blak3mill3r/emacs-ludicrous-speed/-/issues/new) or PR). I am using Debian 10 (buster) and some of these things will differ on other distros.

If you are using `systemd`, as most of us are, you will probably need to [disable journald](https://forums.fedoraforum.org/showthread.php?292543-how-to-permanently-disable-systemd-journald-service), because otherwise the open socket to `journald` from `emacs` will break the checkpoint/restore. This could change in the future, and there might be a way to make it work, but I personally don't mind disabling `journald` so that is the only solution I know of.

You need these deps:

* [CRIU](https://criu.org/Packages)
* [criu-ns](https://criu.org/CR_in_namespace) script from `criu-dev` branch, here is a one-liner:
 ```bash
 sudo su -c "$(cat <<-END
 cd /usr/local/sbin &&
 curl -O https://raw.githubusercontent.com/checkpoint-restore/criu/criu-dev/scripts/criu-ns &&
 chmod u+x criu-ns
 END
 )"
 ```
* [babashka](https://github.com/borkdude/babashka#quickstart) is the runtime of the `els-cloner` service, the `bb` binary must be available to `root`
* [tmux](https://github.com/tmux/tmux/wiki/Installing) to work around a snag with `emacsclient`
* [lsof](https://linux.die.net/man/8/lsof) to detect which process is listening on a socket
* [python](https://www.python.org/) for `criu-ns`, which you almost certainly have already
* [start-stop-daemon](https://man7.org/linux/man-pages/man8/start-stop-daemon.8.html) which you may have already

#### Then to install, first clone this repo:

```bash
ELSHOME=~/.els
git clone https://gitlab.com/blak3mill3r/emacs-ludicrous-speed.git $ELSHOME && cd $ELSHOME
```

#### Put the scripts on your `$PATH` somehow, for example by adding this to your `~/.bashrc`:

```bash
export ELSHOME=~/.els
export PATH=$PATH:$ELSHOME/bin
```

#### Tell emacs to load the included elisp code (eagerly). For example, add this to `init.el`

```elisp
(load (substitute-in-file-name "$ELSHOME/elisp/emacs-ludicrous-speed.el"))
```

#### Configure the service

There is a config file `/etc/els-cloner.edn`, and a script is provided to help set it up:

```bash
els-configure
```

This expects `$XDG_RUNTIME_DIR` and will detect the `emacs` binary on your `$PATH`, then write the config file. You may wish to review/edit the config file now.

#### Make sure you do not have an `emacs --daemon` process listening on the default socket.

You might want to exit all instances of `emacs` at this point.

#### Install the service and the init script as `root`, something like this (but with your own username instead of `you`):

```bash
sudo su -
export ELSHOME=/home/you/.els
cp $ELSHOME/bin/els-cloner /sbin/ && chmod 0744 /sbin/els-cloner
cp --preserve=mode $ELSHOME/init.d/els-cloner /etc/init.d/
update-rc.d els-cloner defaults
service els-cloner start
```

#### The first time the service starts, it will try to ["bake" a fresh emacs snapshot for you](#rebaking). This will take a few seconds.

If it worked, then `/var/log/els-cloner.log` is going to say something like

```
New warm pid =  23791
```

And you will notice there is an `emacs --daemon` process running with that pid.

### Now for the fun part...

[![ludicrous speed](ludicrous-speed.gif)](https://www.youtube.com/watch?v=ygE01sOhzz0)

Execute this (and hold on to your helmet):

```bash
els-clone-one foo somefile
```

Where `foo` is a unique identifier for one cloned instance of emacs (which also determines the socket name).

If you are curious just how fast this is, you can measure it like this:

```bash
els-clone-one foo -e "(message (els-benchmark $(date +'%s.%N')))"
```

Then check a few lines up in the `*Messages*` buffer. On mine it says:

```
Starting Emacs daemon.
foo
Restarting server
Cloned in 305 milliseconds
```

That 305ms is the time from when bash checks the system time (just prior to calling `els-clone-one`) to the time that the fresh emacs clone evaluated our argument.

## Troubleshooting

Good luck to you. I am willing to help, [submit an issue](https://gitlab.com/blak3mill3r/emacs-ludicrous-speed/-/issues/new). Things you might want to try:
* `/var/log/els-cloner.log`
* `$ELSHOME/.images/mine/dump.log`
* `/var/log/els-cloner-restore.log`
* read the source code
* use the schwartz

## Next steps

A wrapper script called `els-emacs` is provided which attempts to simulate the experience of launching `emacs` by executing the actual `emacs` binary.
If you are ready to commit, and want to type `emacs` in a terminal and have it launch a clone instead, you will want to put that script on your `$PATH` taking precedence over the actual `emacs` binary.
Supposing you have a `~/bin` directory which is early on your `$PATH`, you might do something like this:

```bash
EMACSBIN=$(which emacs)
cd ~/bin
ln -s $EMACSBIN emacs-slow
ln -s $ELSHOME/bin/els-emacs ./emacs
```

Now you can run `emacs-slow` to run it the old way, and `emacs` to run it the fast way. There are [caveats](#command-line-caveats) regarding command line arguments, but at least the following will work:

```bash
emacs myfile
emacs -e '(message "They've gone to plaid.")'
```

When launching this way, a randomly generated name is given to the clone. To provide a name for the clone, use `els-clone-one` instead of `emacs`.

When you delete the last frame in one of these clones, the process will exit. This is to mimic the experience of running multiple plain `emacs` instances.

## Rebaking

A freshly launched clone of `emacs --daemon` has in memory the `elisp` that it evaluated when the `els-cloner` service baked the snapshot.
That means that if you modify your config, these changes will not take effect in clones until you rebake, thusly:

```bash
els-rebake
```

This might take a few seconds, and will log to `/var/log/els-cloner.log`

There is currently no automation around rebaking; this would be [a useful addition](#room-for-improvement). I modify my own emacs config maybe once every few days, so this is not much of a headache for me.

## Usage

The experience is very similar to launching a plain `emacs` binary. Each clone has an isolated environment and has already loaded your config.

You can also take advantage of the sockets being named after the unique identifier passed to `els-clone-one`.

This will let you pass commands to a particular clone, for example:

```bash
els-clone-one quux
emacsclient -s $XDG_RUNTIME_DIR/emacs/quux -e '(message "Hold on to your helmet!")'
# or equivalently:
els-eval quux '(message "Hold on to your helmet!")'
```

## Adapting your emacs config

This package makes deferred loading unnecessary. In fact, it is counterproductive.

Instead of deferring as much evaluation as possible to reduce start-up time,
you now want to eagerly evaluate everything so that your experience using `emacs` is faster.

I like to use [`straight.el`](https://github.com/raxod502/straight.el) and [`use-package`](https://github.com/jwiegley/use-package) to manage my config.
So, I simply add [`:demand t`](https://github.com/jwiegley/use-package#notes-about-lazy-loading) to each package declaration
to load it eagerly. This system ought to work fine regardless of how you choose to manage your config.

Deferred loading will still work fine, but it is only slowing down your experience (well, also saving a little memory).

## Command line caveats

Since what is really happening with your arguments is that they are passed to `emacsclient` talking to the socket of a fresh clone, they are not equivalent to the command line arguments of `emacs` itself. There are many arguments to `emacs` which are not supported by `emacsclient`, and these will not work. If you want to adjust the way the `emacs` binary is launched for cloning, just edit `~/.els/bin/els-rebake-launch-emacs`.

## Restoring a desktop

The main way I tend to use this is by having it start clones which immediately restore an [emacs desktop](https://www.gnu.org/software/emacs/manual/html_node/emacs/Saving-Emacs-Sessions.html) (actually, I have my window manager doing this for me when I visit certain workspaces).

```bash
els-clone-one some-project --eval '(desktop-read "/home/blake/.emacs.d/desktops/some-project-code")'
```

Then when I am done working on that project, I `(save-desktop)` and quit emacs. The next time I visit that workspace, it's all back again in a flash.

## Further musings

My own config contains many packages for working with lots of different programming languages. Of course I do not need all the Haskell goodies when I am working in Clojure, and this is one of the problems which deferred loading attempts to address.

However, when you tell CRIU to restore a snapshot of a checkpointed process, all it has to do is copy memory pages from your disk into memory and then do its black-magic to resume the process.

The time it takes to do this does not increase very much as you add more elisp your config. It is probably roughly linear with the amount of virtual memory that a freshly-launched `emacs --daemon` takes.

Also, this latency is hidden most of the time because `els-cloner` does not wait for you to ask for a new clone to do the CRIU restore.
Instead, it always keeps a "warm" one running (listening on the default socket, called `server`).
When you request a new clone, this running daemon is told via `emacsclient` to change the value of the
[`server-name`](https://www.gnu.org/software/emacs/manual/html_node/emacs/Emacs-Server.html) var and then
run [`server-start`](https://www.gnu.org/software/emacs/manual/html_node/emacs/Emacs-Server.html).
Now it is listening on a different socket. Then `emacsclient` is used a second time, with that named socket, to forward arguments and display a frame.

The moment the "warm" one runs `start-server` with the new `server-name`, it relinquishes the default `server` socket;
at that moment it becomes possible to CRIU-restore a replacement, and `els-cloner` does this immediately.

The upshot is the following delightfully freeing fact:
> **Regardless of how many packages you install, or how much elisp you have in your config, you do not add any latency to the cloning of emacsen.**

That is, unless you try to start a clone right after starting a clone. Strictly speaking, there is a very brief period of time during which you might see latency while CRIU restores the snapshot to replace the warm emacs process.

The `els-cloner` service queues commands, so even if you do request another clone in this short window during which
you do not have a warm one running, it will still work the same way, but with a tiny bit of additional latency.
You would only notice this if you request several clones in quick succession. There is [room to improve](#room-for-improvement) on this as well.

If I launch 10 of them simultaneously, it takes a total of 4.4s. You can measure that like this:

```bash
export TS=$(date +'%s.%N') && for i in {1..9}; do emacs; done && emacs -e "(message (els-benchmark $TS))"
```

FWIW, my current snapshot on disk is 92Mb. A freshly started clone takes 329Mb of virtual memory, 96Mb resident. YMMV.

## Room for improvement

Here are some things that you might want it to do, which I think it could do, but which it does not do:

* support multiple linux users
  * This seems relatively easy to add, but I haven't bothered because I am the only user of my workstation
  * Each user would need their own snapshots and warm emacsen
* help users to install/configure it
* some automation around rebaking would be nice, such as:
  * it could watch for changes to any `*.el` file and enqueue a rebake (after a short delay, i.e. debounce)
  * while rebaking, clone commands are enqueued; this could be improved so that you can still clone while rebaking, with the last image
  * if you muck up your config and rebake, the old CRIU image is gone; it would be nice if the last few were kept around so you can roll back to the last working version and still have the speed (currently you'd have to roll back the change to your config and rebake, or start the slow way and maybe `--debug-init`). Currently I just add some elisp and evaluate it, then rebake once it is working.
* support keeping more than one warm emacs at the ready
  * I have my window manager launch multiple separate emacsen simultaneously sometimes, on different workspaces, so I do notice this extra latency.
  * since the server sockets can only be owned by one process, and CRIU-restore will restore a process with a particular socket (the one listening when CRIU checkpointed the process), the `els-cloner` service would have to CRIU-restore three of them in named slots, renaming each one right after restoring it, and then round-robin the clone requests across the ready slots.
* Some of the command line arguments to `emacs` will not work with this trickery since you're really passing them to `emacsclient`. Some of that is inevitable, some of it perhaps can be improved.
* support non-graphical emacs in a terminal
  * this is probably not too hard to add, but I never use this, so I don't care much.
* stop requiring `lsof`?
  * the same thing could be done by reading something in `/proc` I suspect
* stop requiring `tmux`
  * currently it does not work without `tmux` and I do not understand why (see source code)

## Isolation

Each emacs clone is entirely independent of the others, just as though you had launched multiple instances of emacs in the normal way.

This is notably different from running one emacs with many frames, which would use less memory and also share the elisp environment.

I have tried to work that way and I found it annoying. Most of the time I want separate projects to have separate emacs environments. I want separate sets of buffers and I don't have much use for sharing vars across the projects.
Also, when it is necessary to restart emacs, this approach becomes cumbersome and slow, especially using client/server.

Of course, you can still make as many frames as you want in each cloned emacs instance.

## Contributing

Just open a PR on GitLab. Thanks! :heart:

## License
[MIT](LICENSE)

Do as you please.
